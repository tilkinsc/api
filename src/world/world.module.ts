import { Module } from '@nestjs/common';
import { WorldDataService } from 'src/world/world.service';
import { WorldMongoModule } from './world-mongo.module';
import { WorldController } from './world.controller';

@Module({
  imports: [WorldMongoModule],
  controllers: [WorldController],
  providers: [WorldDataService],
  exports: [WorldDataService],
})
export class WorldDataModule {}
