import { DynamicModule, Logger, Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';

@Module({
  imports: [ConfigModule],
})
export class WorldMongoModule {
  private static readonly logger = new Logger(WorldMongoModule.name);

  static forRoot(): DynamicModule {
    const prefix = 'WORLD_MONGO_URI_';
    const connectionProviders = [];

    for (const key in process.env) {
      if (key.startsWith(prefix)) {
        const connectionName = `${key.substring(prefix.length).toLowerCase()}Connection`;
        connectionProviders.push(
          MongooseModule.forRootAsync({
            imports: [ConfigModule],
            connectionName,
            useFactory: async (configService: ConfigService) => {
              const uri = configService.get<string>(key);
              if (!uri) throw new Error(`URI for ${connectionName} not found.`);
              return { uri };
            },
            inject: [ConfigService],
          }),
        );
        WorldMongoModule.logger.log('Connection provider pushed: ' + connectionName);
      }
    }

    return {
      module: WorldMongoModule,
      imports: connectionProviders,
      exports: connectionProviders,
    };
  }
}
