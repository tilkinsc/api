import { Module } from '@nestjs/common';
import { LogsController } from './logs.controller';
import { LogsService } from './logs.service';
import { WorldDataService } from 'src/world/world.service';

@Module({
  imports: [],
  controllers: [LogsController],
  providers: [WorldDataService, LogsService],
})
export class LogsModule {}
