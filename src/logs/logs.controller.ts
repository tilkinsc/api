import { Controller, Get, Param, Query, UseGuards } from '@nestjs/common';
import { LogsService } from './logs.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { RequireRights, Rights, RightsGuard } from 'src/auth/rights';

@Controller('logs')
@UseGuards(JwtAuthGuard, RightsGuard)
export class LogsController {
  constructor(private readonly logsService: LogsService) {}

  @Get(':world/:logType')
  @RequireRights(Rights.ADMIN)
  async getErrors(
    @Param('world') world: string,
    @Param('logType') logType: string,
    @Query('page') page = 1,
    @Query('limit') limit = 25,
  ) {
    return await this.logsService.getLogs(world, page, limit, logType.toUpperCase());
  }
}
