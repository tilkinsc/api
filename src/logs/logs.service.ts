import { Injectable } from '@nestjs/common';
import { WorldDataService } from 'src/world/world.service';

@Injectable()
export class LogsService {
  constructor(private readonly worldData: WorldDataService) {}

  async getLogs(world: string, page = 1, limit = 25, type: string) {
    page = Number(page);
    limit = Number(limit);
    const startIndex = (page - 1) * limit;
    return await this.worldData.getCollection(world, 'logs').find({ type }).skip(startIndex).limit(limit).toArray();
  }
}
