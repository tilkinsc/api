// import { Controller, Get, Param, Query } from '@nestjs/common';
// import { TemporalHiscoreService } from './temporal_hiscore.service';

// @Controller('temporal')
// export class TemporalController {
//   constructor(private readonly temporalHiscoreService: TemporalHiscoreService) {}

//   @Get(':world')
//   async getTotalTemporalHiscore(
//     @Param('world') world: string,
//     @Query('firstDayPast') firstDayPast = 0,
//     @Query('secondDayPast') secondDayPast = 1,
//     @Query('page') page = 1,
//     @Query('limit') limit = 6,
//     @Query('skill') skill = -1
//   ) {
//     return await this.temporalHiscoreService.get(world, firstDayPast, secondDayPast, page, limit, skill);
//   }

//   @Get(':world/player')
//   async getPlayerTemporalHiscore(
//     @Param('world') world: string,
//     @Query('firstDayPast') firstDayPast = 0,
//     @Query('secondDayPast') secondDayPast = 1,
//     @Query('username') username = ''
//   ) {
//     return await this.temporalHiscoreService.getPlayer(world, firstDayPast, secondDayPast, username);
//   }
// }
