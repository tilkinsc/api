import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import slugify from 'slugify';

@Schema({ timestamps: true })
export class Article {
  @Prop({ required: true, index: true, unique: true })
  title: string;

  @Prop({ required: true, index: true, default: 0 })
  type: number;

  @Prop({ required: true })
  description: string;

  @Prop({ required: true })
  markdown: string;

  @Prop()
  sanitizedHtml: string;

  @Prop({ index: true })
  slug: string;
}

export const ArticleSchema = SchemaFactory.createForClass(Article);

ArticleSchema.pre('save', function (next) {
  if (this.isModified('title')) this.slug = slugify(this.title, { lower: true, strict: true });
  if (this.isModified('markdown')) this.sanitizedHtml = this.markdown;
  next();
});
