import * as crypto from 'crypto';

export class Crypt {
  static encrypt(secret: string, salt: number[], plainText: string) {
    const key = crypto.pbkdf2Sync(secret, Buffer.from(salt), 65536, 32, 'sha256');
    const iv = Buffer.from([0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);

    const cipher = crypto.createCipheriv('aes-256-cbc', key, iv);
    const encrypted = Buffer.concat([cipher.update(plainText, 'utf8'), cipher.final()]);

    return Array.from(Int8Array.from(Buffer.from(encrypted)));
  }
}
