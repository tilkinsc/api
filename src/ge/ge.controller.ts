import { Controller, Get, Param, Query } from '@nestjs/common';
import { GEService } from './ge.service';

@Controller('ge')
export class GEController {
  constructor(private readonly geService: GEService) {}

  @Get(':world')
  async getAllOffers(@Param('world') world: string, @Query('page') page = 1, @Query('limit') limit = 25) {
    return await this.geService.getAllOffers(world, page, limit);
  }

  @Get(':world/buy')
  async getAllBuys(@Param('world') world: string, @Query('page') page = 1, @Query('limit') limit = 25) {
    return await this.geService.getAllOffers(world, page, limit, 'BUY');
  }

  @Get(':world/sell')
  async getAllSell(@Param('world') world: string, @Query('page') page = 1, @Query('limit') limit = 25) {
    return await this.geService.getAllOffers(world, page, limit, 'SELL');
  }
}
