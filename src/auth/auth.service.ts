import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { Crypt } from 'src/crypt';
import { AccountsService } from '../accounts/accounts.service';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private config: ConfigService,
    private accountsService: AccountsService,
    private jwtService: JwtService,
  ) {}

  async validateUser(username: string, pass: string): Promise<any> {
    const secret = this.config.get<string>('PASS_CRYPT_SECRET')!;
    const salt = this.config.get<string>('PASS_CRYPT_SALT')!;
    const saltArray = salt.split(',').map(num => parseInt(num, 10));
    const user = await this.accountsService.getByUserOrEmail(username);
    if (user && JSON.stringify(user.password) == JSON.stringify(Crypt.encrypt(secret, saltArray, pass))) {
      const { ...result } = user;
      return result;
    }
    return null;
  }

  async login(user: any) {
    const payload = { username: user.username, sub: user.userId, rights: user.rights };
    return {
      access_token: this.jwtService.sign(payload),
    };
  }
}
